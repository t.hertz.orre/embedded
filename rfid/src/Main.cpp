#include <Keyboard.h>
#include <MFRC522.h>
#include <SPI.h>

#define RESET_PIN 19
#define SDA_PIN 10

// Create MFRC522 instance
MFRC522 mfrc522(SDA_PIN, RESET_PIN);


void setup()
{
    // Initialize control over the keyboard.
    Keyboard.begin(KeyboardLayout_sv_SE);

    // Init SPI bus.
    SPI.begin();

    // Init MFRC522 device.
    mfrc522.PCD_Init();
}

void LogStuff(MFRC522::Uid *uid)
{
    Keyboard.print("<RFID_");

    for (byte i = 0; i < uid->size; i++)
    {
        if (uid->uidByte[i] < 0x10)
            Keyboard.print("0");

        // else
        //     Keyboard.print('');

        Keyboard.print(uid->uidByte[i], HEX);
    }

    Keyboard.print('>');
}

void loop()
{
    // No new card present.
    if (!mfrc522.PICC_IsNewCardPresent())
    {
        return;
    }

    // Select one of the cards
    if (!mfrc522.PICC_ReadCardSerial())
    {
        return;
    }

    LogStuff(&(mfrc522.uid));

    mfrc522.PICC_HaltA();
}
