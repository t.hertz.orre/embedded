#define TURBO_DURATION_CYCLES 125
#define TURBO_PAUSE_CYCLES 125
#define AXIS_RADIUS 512
#define PORT_COUNT 1
#define JOYSTICK_COUNT 1

#define CONNECT_COOLDOWN 1000
#define BUTTON_COUNT_NES 8
#define BUTTON_COUNT_SNES 12
#define BUTTON_COUNT 12
#define RAW_DATA_COUNT 16

#define CYCLE_COUNT_ID 8
#define CYCLE_COUNT_NES 8
#define CYCLE_COUNT_SNES 16
#define CYCLE_COUNT_ARCADE24 24

#define PULSE_WIDTH_CD4021 20
#define PULSE_WIDTH_HC165 5

uint8_t bitsToInteger(const bool idArray[8])
{
    uint8_t result = 0;
    uint8_t shift = 7;

    for (int i = 0; i < 8; ++i) result += idArray[i] << shift--;
    return result;
}

const uint8_t ID_NES        = bitsToInteger((bool[]){ 0, 0, 0, 0, 0, 0, 0, 0 });    // ID LOW, interpret as 8 cycle NES controller.
const uint8_t ID_SNES       = bitsToInteger((bool[]){ 1, 1, 1, 1, 1, 1, 1, 1 });    // ID HIGH, interpret as 16 cycle SNES controller with 12 buttons.
const uint8_t ID_SNES_HC    = bitsToInteger((bool[]){ 0, 0, 0, 0, 1, 1, 1, 1 });    // HC165 ID is cheaper to do with one side LOW and one side HIGH.
const uint8_t ID_ARCADE24   = bitsToInteger((bool[]){ 0, 0, 0, 0, 0, 0, 0, 1 });    // My 24-bit 2-player arcade board. 0.12ms to 1ms latency.



/*
    // Convert bits to degrees.
    int hatLinear = (right * up) + (right * !up * 3) + (down * right) + (down * !right * 5) + (left * down) + (left * !down * 7) + (up * left) + (up * !left) - 1;
    int hat = (hatLinear < 0) ? -1 : hatLinear * 45;

    // Set Hat
    device.setHatSwitch(0, hat);
*/