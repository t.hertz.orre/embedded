#include <Joystick.h>
#include <Arduino.h>
#include <Nes.h>

#define DEBUG true

// Pins
#define CLOCK_PIN A0
#define LATCH_PIN A1
#define DATA_PIN A2
#define ID_PIN A3

enum InputState
{
    disconnected,
    connected,
    waiting
};

// Create Joystick
Joystick_ usbJoysticks[1] =
{
    Joystick_(0x03, JOYSTICK_TYPE_GAMEPAD, BUTTON_COUNT - 4, 0, true, true, false, false, false, false, false, false, false, false, false),
    // Joystick_(0x04, JOYSTICK_TYPE_GAMEPAD, BUTTON_COUNT - 4, 0, true, true, false, false, false, false, false, false, false, false, false),
};

// The inputRaw has to be the same or greater length then cycle amount.

bool inputBuffer[BUTTON_COUNT][JOYSTICK_COUNT];
bool inputRaw[RAW_DATA_COUNT][JOYSTICK_COUNT];
bool deviceIdRaw[CYCLE_COUNT_ID];
InputState inputState;
uint8_t buttonCount = 8;
uint8_t cycleCount = 8;
uint8_t deviceId;
int8_t cycle = -1;

#if DEBUG
// uint32_t timeSpentSleeping = 0;
// unsigned long millisLast = 0;
uint32_t inputUpdates = 0;

void printBuffer()
{
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.print("    Updates ");
    Serial.print(inputUpdates);
    Serial.println();

    for (size_t y = 0; y < 1; ++y)
    {
        Serial.println();
        Serial.print(y);
        Serial.print("   ");

        for (size_t x = 0; x < RAW_DATA_COUNT; ++x)
        {
            Serial.print("[ ");
            Serial.print(inputRaw[x][y]);
            Serial.print(" ]");
        }
    }

    Serial.print("     ");

    for (size_t i = 0; i < CYCLE_COUNT_ID; ++i)
    {
        Serial.print("[ ");
        Serial.print(deviceIdRaw[i]);
        Serial.print(" ]");
    }

    Serial.println();
    Serial.print("    ");

    for (size_t i = 0; i < buttonCount; ++i)
    {
        Serial.print("[ ");

        if (!inputBuffer[i][0]) Serial.print(" ");

        else if (deviceId == ID_NES)
        {
            switch (i)
            {
                case 0: Serial.print("A"); break;
                case 1: Serial.print("B"); break;
                case 2: Serial.print("▬"); break;
                case 3: Serial.print("S"); break;
                case 4: Serial.print("▲"); break;
                case 5: Serial.print("▼"); break;
                case 6: Serial.print("◄"); break;
                case 7: Serial.print("►"); break;
                default: Serial.print("-"); break;
            }
        }

        else if (deviceId == ID_SNES)
        {
            switch (i)
            {
                case 0: Serial.print("B"); break;
                case 1: Serial.print("Y"); break;
                case 2: Serial.print("▬"); break;
                case 3: Serial.print("S"); break;
                case 4: Serial.print("▲"); break;
                case 5: Serial.print("▼"); break;
                case 6: Serial.print("◄"); break;
                case 7: Serial.print("►"); break;
                case 8: Serial.print("A"); break;
                case 9: Serial.print("X"); break;
                case 10: Serial.print("L"); break;
                case 11: Serial.print("R"); break;
                default: Serial.print("-"); break;
            }
        }

        Serial.print(" ]");
    }

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
}
#endif

void setup()
{
    // Pin Setup
    pinMode(CLOCK_PIN, OUTPUT);
    pinMode(LATCH_PIN, OUTPUT);
    pinMode(DATA_PIN, INPUT);

    // Turn off leds.
    pinMode(LED_BUILTIN_TX, INPUT);
    pinMode(LED_BUILTIN_RX, INPUT);

    // Initial pin states.
    digitalWrite(CLOCK_PIN, LOW); // NES control lines idle low
    digitalWrite(LATCH_PIN, LOW);

    // Initiate joystick to send state on manually.
    for (int i = 0; i < JOYSTICK_COUNT; ++i)
    {
        // Tell gamepads to send state manually.
        usbJoysticks[i].begin(false);

        // Adjusting max range to 1024 translates better into games made in by Unity.
        usbJoysticks[i].setXAxisRange(0, AXIS_RADIUS * 2);
        usbJoysticks[i].setYAxisRange(0, AXIS_RADIUS * 2);
    }

    #if DEBUG
    Serial.begin(9600);
    #endif
}

bool isDisconnected()
{
    for (size_t y = 0; y < 1; ++y)
        for (size_t x = 0; x < RAW_DATA_COUNT; ++x)
            if (inputRaw[x][y]) return false;

    return true;
}

void clearRawInput()
{
    for (size_t y = 0; y < 1; ++y)
        for (size_t x = 0; x < RAW_DATA_COUNT; ++x)
            inputRaw[x][y] = false;
}

void clearInputOnUSB(uint8_t joystickIndex = 0)
{
    usbJoysticks[joystickIndex].setXAxis(AXIS_RADIUS);
    usbJoysticks[joystickIndex].setYAxis(AXIS_RADIUS);
    for (size_t i = 0, n = BUTTON_COUNT - 4; i < n; ++i)
        usbJoysticks[joystickIndex].setButton(i, 0);

    usbJoysticks[joystickIndex].sendState();
}

void transferInputToUsb(uint8_t joystickIndex)
{
    // Direction Vector
    int axisX = (int)inputBuffer[7][joystickIndex] - (int)inputBuffer[6][joystickIndex];
    int axisY = (int)inputBuffer[5][joystickIndex] - (int)inputBuffer[4][joystickIndex];

    // Direction Normalized
    int axisNormalizedX = (axisX * AXIS_RADIUS) + AXIS_RADIUS;
    int axisNormalizedY = (axisY * AXIS_RADIUS) + AXIS_RADIUS;

    // Set joystick axis.
    Joystick_ joystick = usbJoysticks[joystickIndex];
    joystick.setXAxis(axisNormalizedX);
    joystick.setYAxis(axisNormalizedY);

    // Set Nintendo buttons.
    if (deviceId == ID_NES || deviceId == ID_SNES)
    {
        joystick.setButton(0, inputBuffer[0][joystickIndex]);
        joystick.setButton(1, inputBuffer[1][joystickIndex]);
        joystick.setButton(2, inputBuffer[2][joystickIndex]);
        joystick.setButton(3, inputBuffer[3][joystickIndex]);
    }

    // Set SNES buttons.
    if (deviceId == ID_SNES)
    {
        joystick.setButton(4, inputBuffer[8][joystickIndex]);
        joystick.setButton(5, inputBuffer[9][joystickIndex]);
        joystick.setButton(6, inputBuffer[10][joystickIndex]);
        joystick.setButton(7, inputBuffer[11][joystickIndex]);
    }

    // Send State
    joystick.sendState();
}

// TODO Move these to some sort of c++ utils.
uint8_t bitsToInteger(bool idArray[8])
{
    uint8_t result = 0;
    uint8_t shift = 7;

    for (int i = 0; i < 8; ++i) result += idArray[i] << shift--;
    return result;
}

bool wasInputUpdated(uint8_t joystickIndex, uint16_t cycle, bool state)
{
    // Skip buttons not on device.
    if (cycle >= buttonCount) return false;

    // Update raw input.
    inputRaw[cycle][joystickIndex] = state;

    // Was input updated.
    if (inputBuffer[cycle][joystickIndex] == state)
    {
        inputBuffer[cycle][joystickIndex] = !state;
        return true;
    }

    return false;
}

bool wasIdUpdated(uint16_t cycle, bool state)
{
    // Only return on end of ID register.
    deviceIdRaw[cycle] = state;
    if (cycle != 7) return false;

    // Only return on updated ID.
    uint8_t id = bitsToInteger(deviceIdRaw);
    if (deviceId == id) return false;

    // Return on new ID.
    deviceId = id;
    return true;
}

void setDeviceParameters()
{
    clearRawInput();

    if (deviceId == ID_NES)
    {
        #if DEBUG
        Serial.println();
        Serial.print("NES found [");
        Serial.print(deviceId);
        Serial.print("]");
        Serial.println();
        #endif

        buttonCount = BUTTON_COUNT_NES;
        cycleCount = CYCLE_COUNT_NES;
        return;
    }

    if (deviceId == ID_SNES)
    {
        #if DEBUG
        Serial.println();
        Serial.print("SNES found [");
        Serial.print(deviceId);
        Serial.print("]");
        Serial.println();
        #endif

        buttonCount = BUTTON_COUNT_SNES;
        cycleCount = CYCLE_COUNT_SNES;
        return;
    }
}

bool pulseRegisters()
{
    // Increment Cycle
    ++cycle;
    if (cycle >= cycleCount) cycle = 0;

    // Latch register(s) at start.
    if (cycle == 0)
    {
        digitalWrite(LATCH_PIN, HIGH);
        delayMicroseconds(PULSE_WIDTH_CD4021);
        digitalWrite(LATCH_PIN, LOW);
    }

    // Read cycle data.
    bool updated = wasInputUpdated(0, cycle, (bool)digitalRead(DATA_PIN));

    // Read cycle id.
    if (wasIdUpdated(cycle, (bool)digitalRead(ID_PIN))) setDeviceParameters();

    // Pulse
    digitalWrite(CLOCK_PIN, HIGH);
    delayMicroseconds(PULSE_WIDTH_CD4021);
    digitalWrite(CLOCK_PIN, LOW);

    return updated;
}

void loop()
{
    // Check Input
    if (pulseRegisters())
    {
        transferInputToUsb(0);

        #if DEBUG
        ++inputUpdates;
        // timeSpentSleeping = 0;
        printBuffer();
        #endif
    }

    if (isDisconnected())
    {
        // On Disconnect
        if (inputState == connected)
        {
            #if DEBUG
            Serial.println("disconnected");
            inputUpdates = 0;
            #endif

            inputState = disconnected;
            cycle = -1;

            clearInputOnUSB();
            delay(CONNECT_COOLDOWN);
            return;
        }

        // On Waiting
        #if DEBUG
        Serial.println("waiting");
        #endif

        inputState = waiting;
        delay(CONNECT_COOLDOWN);
    }

    // On Connect
    else if (inputState == disconnected || inputState == waiting)
    {
        #if DEBUG
        Serial.println("connected");
        #endif

        inputState = connected;
    }
}