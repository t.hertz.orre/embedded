#include <Joystick.h>
#include <Arduino.h>
#include <Saturn.h>

#define DEBUG false

// Input
#define DATA_0_PIN A0
#define DATA_1_PIN A1
#define DATA_2_PIN A2
#define DATA_3_PIN A3

// Output
#define SELECT_0_PIN 16
#define SELECT_1_PIN 14
#define ONLINE_PIN 10
#define LED_RED 3
#define LED_GREEN 5
#define LED_BLUE 6

bool inputRaw[SATURN_DATA_PINS][SATURN_CYCLE_COUNT];
bool inputBuffer[SATURN_BUTTON_COUNT][SATURN_GAMEPAD_COUNT];

int8_t cycle = -1;
uint64_t inputCooldown = 0;
uint8_t ledState = 0;

#if DEBUG
unsigned long millisLast = 0;
uint32_t timeSpentSleeping = 0;
uint32_t inputUpdates = -1;
#endif

// Allocate USB Joysticks
Joystick_ usbJoysticks[SATURN_GAMEPAD_COUNT] =
{
    Joystick_(0x03, JOYSTICK_TYPE_GAMEPAD, SATURN_BUTTON_COUNT - 4, 0, true, true, false, false, false, false, false, false, false, false, false),
};

void setup()
{
    // Pin Setup
    pinMode(DATA_0_PIN,   INPUT);
    pinMode(DATA_1_PIN,   INPUT);
    pinMode(DATA_2_PIN,   INPUT);
    pinMode(DATA_3_PIN,   INPUT);
    pinMode(SELECT_0_PIN, OUTPUT);
    pinMode(SELECT_1_PIN, OUTPUT);
    pinMode(ONLINE_PIN,   OUTPUT);
    pinMode(LED_RED,      OUTPUT);
    pinMode(LED_GREEN,    OUTPUT);
    pinMode(LED_BLUE,     OUTPUT);

    // Turn off leds.
    pinMode(LED_BUILTIN_TX, INPUT);
    pinMode(LED_BUILTIN_RX, INPUT);

    // Initial pin write states.
    digitalWrite(ONLINE_PIN, HIGH);

    // Initiate joystick to send state on manually.
    for (int i = 0; i < SATURN_GAMEPAD_COUNT; ++i)
    {
        // Tell gamepads to send state manually.
        usbJoysticks[i].begin(false);

        // Adjusting max range to 1024 translates better into games made in by Unity.
        usbJoysticks[i].setXAxisRange(0, SATURN_AXIS_RADIUS * 2);
        usbJoysticks[i].setYAxisRange(0, SATURN_AXIS_RADIUS * 2);
    }

    // Initiate input buffer with false positive as first "history".
    for (size_t i = 0; i < SATURN_GAMEPAD_COUNT; ++i)
        inputBuffer[0][i] = true;

#if DEBUG
    Serial.begin(9600);
#endif
}

void setColor(int R, int G, int B)
{
    analogWrite(LED_RED,   R);
    analogWrite(LED_GREEN, G);
    analogWrite(LED_BLUE,  B);
}

void transferInputToUsb(uint8_t joystickIndex)
{
    // Direction Vector
    int axisX = inputBuffer[3][joystickIndex] - inputBuffer[2][joystickIndex];
    int axisY = inputBuffer[1][joystickIndex] - inputBuffer[0][joystickIndex];

    // Direction Normalized
    int axisNormalizedX = (axisX * (SATURN_AXIS_RADIUS)) + SATURN_AXIS_RADIUS;
    int axisNormalizedY = (axisY * (SATURN_AXIS_RADIUS)) + SATURN_AXIS_RADIUS;

    // Set D-pad axis.
    Joystick_ joystick = usbJoysticks[joystickIndex];
    joystick.setXAxis(axisNormalizedX);
    joystick.setYAxis(axisNormalizedY);

    // Set button states.
    size_t buttonOffset = 4;
    for (size_t i = buttonOffset; i < SATURN_BUTTON_COUNT; ++i)
        joystick.setButton(i - buttonOffset, inputBuffer[i][joystickIndex]);

    // Send state to usb host.
    joystick.sendState();
}

bool updateInput(uint8_t joystickIndex)
{
    bool wasUpdated = false;
    bool isUp = !inputRaw[0][2];
    bool isDown = !inputRaw[1][2];
    bool isLeft = !inputRaw[2][2];
    bool isRight = !inputRaw[3][2];
    bool isButtonA = !inputRaw[2][1];
    bool isButtonB = !inputRaw[0][1];
    bool isButtonC = !inputRaw[1][1];
    bool isButtonStart = !inputRaw[3][1];
    bool isButtonX = !inputRaw[2][0];
    bool isButtonY = !inputRaw[1][0];
    bool isButtonZ = !inputRaw[0][0];
    bool isButtonShoulderLeft = !inputRaw[3][3];
    bool isButtonShoulderRight = !inputRaw[3][0];

    if (inputBuffer[0][joystickIndex] != isUp) { inputBuffer[0][joystickIndex] = isUp; wasUpdated = true; }
    if (inputBuffer[1][joystickIndex] != isDown) { inputBuffer[1][joystickIndex] = isDown; wasUpdated = true; }
    if (inputBuffer[2][joystickIndex] != isLeft) { inputBuffer[2][joystickIndex] = isLeft; wasUpdated = true; }
    if (inputBuffer[3][joystickIndex] != isRight) { inputBuffer[3][joystickIndex] = isRight; wasUpdated = true; }
    if (inputBuffer[4][joystickIndex] != isButtonA) { inputBuffer[4][joystickIndex] = isButtonA; wasUpdated = true; }
    if (inputBuffer[5][joystickIndex] != isButtonB) { inputBuffer[5][joystickIndex] = isButtonB; wasUpdated = true; }
    if (inputBuffer[6][joystickIndex] != isButtonC) { inputBuffer[6][joystickIndex] = isButtonC; wasUpdated = true; }
    if (inputBuffer[7][joystickIndex] != isButtonStart) { inputBuffer[7][joystickIndex] = isButtonStart; wasUpdated = true; }
    if (inputBuffer[8][joystickIndex] != isButtonX) { inputBuffer[8][joystickIndex] = isButtonX; wasUpdated = true; }
    if (inputBuffer[9][joystickIndex] != isButtonY) { inputBuffer[9][joystickIndex] = isButtonY; wasUpdated = true; }
    if (inputBuffer[10][joystickIndex] != isButtonZ) { inputBuffer[10][joystickIndex] = isButtonZ; wasUpdated = true; }
    if (inputBuffer[11][joystickIndex] != isButtonShoulderLeft) { inputBuffer[11][joystickIndex] = isButtonShoulderLeft; wasUpdated = true; }
    if (inputBuffer[12][joystickIndex] != isButtonShoulderRight) { inputBuffer[12][joystickIndex] = isButtonShoulderRight; wasUpdated = true; }

    return wasUpdated;
}

bool cycleMultiplexer(uint8_t joystickIndex)
{
    ++cycle;
    if (cycle >= SATURN_CYCLE_COUNT) cycle = 0;

    digitalWrite(SELECT_0_PIN, (cycle % 2 == 0) ? LOW : HIGH);
    digitalWrite(SELECT_1_PIN, (cycle < 2) ? LOW : HIGH);

    // Update buffer here for now.
    inputRaw[0][cycle] = (bool)digitalRead(DATA_0_PIN);
    inputRaw[1][cycle] = (bool)digitalRead(DATA_1_PIN);
    inputRaw[2][cycle] = (bool)digitalRead(DATA_2_PIN);
    inputRaw[3][cycle] = (bool)digitalRead(DATA_3_PIN);

    // Only return updated on cycle complete and actual change of input.
    bool wasUpdated = false;
    if (cycle == SATURN_CYCLE_COUNT - 1)
        wasUpdated = updateInput(joystickIndex);

    return wasUpdated;
}

#if DEBUG
void printBuffer()
{
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.print("    Updates ");
    Serial.print(inputUpdates);
    Serial.print(", latency ");
    float cycleDurationMS = 1000 / (float)((SATURN_INPUT_COOLDOWN / 60) / SATURN_CYCLE_COUNT);
    Serial.print(cycleDurationMS);
    Serial.print(" ms to 1 ms.");
    Serial.println();

    for (size_t y = 0; y < SATURN_CYCLE_COUNT; ++y)
    {
        Serial.println();
        Serial.print(y);
        Serial.print("   ");

        for (size_t x = 0; x < SATURN_DATA_PINS; ++x)
        {
            Serial.print("[ ");
            Serial.print(inputRaw[x][y]);
            Serial.print(" ]");
        }
    }

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
}
#endif

bool isDisconnected()
{
    for (size_t y = 0; y < SATURN_CYCLE_COUNT; ++y)
        for (size_t x = 0; x < SATURN_DATA_PINS; ++x)
            if (inputRaw[x][y]) return false;

    return true;
}

void delayInput(uint64_t ms)
{
    cycle = -1;
    delay(ms);
}

void resetJoystick(Joystick_ joystick)
{
    joystick.setXAxis(SATURN_AXIS_RADIUS);
    joystick.setYAxis(SATURN_AXIS_RADIUS);
    for (size_t i = 0; i < SATURN_BUTTON_COUNT - 4; ++i)
        joystick.setButton(i, 0);

    joystick.sendState();
}

void loop()
{
    bool wasUpdated = false;
    for (uint8_t i = 0; i < SATURN_GAMEPAD_COUNT; ++i)
        if (cycleMultiplexer(i))
            wasUpdated = true;

    if (wasUpdated)
    {
        inputCooldown = SATURN_INPUT_COOLDOWN * SATURN_INPUT_COOLDOWN_MINUTES;
        ledState = 1;

#if DEBUG
        ++inputUpdates;
        timeSpentSleeping = 0;
        printBuffer();
#endif

        for (uint8_t i = 0; i < SATURN_GAMEPAD_COUNT; ++i)
            transferInputToUsb(i);
    }

    if (isDisconnected())
    {
#if DEBUG
        Serial.println("disconnected");
        inputUpdates = -1;
#endif

        if (ledState > 0) resetJoystick(usbJoysticks[0]);

        ledState = 0;
        delayInput(SATURN_CONNECT_COOLDOWN);
    }

    if (inputCooldown > 0)
    {
        --inputCooldown;

#if DEBUG
        unsigned long ms = millis();
        unsigned long delta = ms - millisLast;
        millisLast = ms;
        timeSpentSleeping += delta;
#endif
    }

    if (inputCooldown == 0 && cycle == SATURN_CYCLE_COUNT - 1)
    {
#if DEBUG
        uint32_t seconds = timeSpentSleeping / 1000;
        uint32_t decimals = timeSpentSleeping - (seconds * 1000);
        Serial.print("sleep after ");
        Serial.print(seconds);
        Serial.print(".");
        Serial.print(decimals);
        Serial.print(" seconds");
        Serial.println();
#endif

        ledState = 2;
        delayInput(SATURN_INPUT_SLEEP);
    }

    switch (ledState)
    {
        // Disconnected
        case 0: setColor(60, 1, 0); break;

        // Ready
        case 1: setColor(10, 35, 0); break;

        // Standby
        case 2: setColor(0, 0, 3); break;
    }
}