#include <Arduino.h>

bool isRX = true;

void setup()
{
    // Initialize RX pin as an output. TX is already defined.
    pinMode(LED_BUILTIN_RX, OUTPUT);
}

void loop()
{
    if (isRX)
    {
        digitalWrite(LED_BUILTIN_RX, HIGH);
        digitalWrite(LED_BUILTIN_TX, LOW);
        delay(1000);
    }

    else
    {
        digitalWrite(LED_BUILTIN_RX, LOW);
        digitalWrite(LED_BUILTIN_TX, HIGH);
        delay(1000);
    }

    isRX = !isRX;
}

// #include <Arduino.h>
// #include <Nes.h>

// #define DEBUG true

// // Pins
// #define DATA_PIN_PLAYER1 18
// #define DATA_PIN_PLAYER2 19
// #define DATA_PIN_PLAYER3 20
// #define DATA_PIN_PLAYER4 21
// #define TWOFOUR_PIN 16
// #define TURBO_A_PIN 1
// #define TURBO_B_PIN 0
// #define CLOCK_PIN 15
// #define LATCH_PIN 14

// // Create Joystick
// Joystick_ usbJoysticks[PORT_COUNT] =
// {
//     Joystick_(0x03, JOYSTICK_TYPE_GAMEPAD, BUTTON_COUNT_SNES, 0, true, true, false, false, false, false, false, false, false, false, false),
//     Joystick_(0x04, JOYSTICK_TYPE_GAMEPAD, BUTTON_COUNT_SNES, 0, true, true, false, false, false, false, false, false, false, false, false),
//     Joystick_(0x05, JOYSTICK_TYPE_GAMEPAD, BUTTON_COUNT_SNES, 0, true, true, false, false, false, false, false, false, false, false, false),
//     Joystick_(0x06, JOYSTICK_TYPE_GAMEPAD, BUTTON_COUNT_SNES, 0, true, true, false, false, false, false, false, false, false, false, false),
// };

// // Bit Positions
// const int A_BUTTON = 0;
// const int B_BUTTON = 1;
// const int SELECT_BUTTON = 2;
// const int START_BUTTON = 3;
// const int UP_BUTTON = 4;
// const int DOWN_BUTTON = 5;
// const int LEFT_BUTTON = 6;
// const int RIGHT_BUTTON = 7;

// short turboDurationsA[PORT_COUNT];
// short turboDurationsB[PORT_COUNT];
// short turboPausesA[PORT_COUNT];
// short turboPausesB[PORT_COUNT];
// uint8_t registersLast[PORT_COUNT];
// uint8_t playerPins[PORT_COUNT];
// uint8_t registers[PORT_COUNT];
// int halfRange = 512;

// // External Buttons
// bool isFourPlayerLast = true;
// bool isFourPlayer = true;
// bool isTurboA = false;
// bool isTurboB = false;

// bool isTrigger = false;
// bool isLight = false;

// void setup()
// {
//     // Case Pins
//     pinMode(TWOFOUR_PIN, INPUT);
//     pinMode(TURBO_A_PIN, INPUT);
//     pinMode(TURBO_B_PIN, INPUT);

//     // Buffer player pins.
//     playerPins[0] = DATA_PIN_PLAYER1;
//     playerPins[1] = DATA_PIN_PLAYER2;
//     playerPins[2] = DATA_PIN_PLAYER3;
//     playerPins[3] = DATA_PIN_PLAYER4;

//     // Player pin modes.
//     for (int i = 0; i < PORT_COUNT; ++i)
//         pinMode(playerPins[i], INPUT);

//     // Pin Modes
//     pinMode(CLOCK_PIN, OUTPUT);
//     pinMode(LATCH_PIN, OUTPUT);

//     // Initial pin states.
//     digitalWrite(CLOCK_PIN, LOW); // NES control lines idle low
//     digitalWrite(LATCH_PIN, LOW);

//     // Send joystick input manually.
//     for (int i = 0; i < PORT_COUNT; ++i)
//     {
//         // Tell gamepads to send state manually.
//         usbJoysticks[i].begin(false);

//         // Adjusting max range to 1024 translates best into games better due to float conversion.
//         usbJoysticks[i].setXAxisRange(0, AXIS_RADIUS * 2);
//         usbJoysticks[i].setYAxisRange(0, AXIS_RADIUS * 2);
//     }

//     #if DEBUG
//     Serial.begin(9600);
//     #endif
// }

// void SendDeviceState(int index, bool isTurboCounted = false)
// {
//     Joystick_ device = usbJoysticks[index];
//     byte nesRegister = registers[index];

//     // Get turbo buttons.
//     bool isButtonA = bitRead(nesRegister, A_BUTTON);
//     bool isButtonB = bitRead(nesRegister, B_BUTTON);

//     if (isTurboCounted)
//     {
//         uint16_t timePassed = PULSE_WIDTH_CD4021 * REGISTER_LENGTH;

//         // Reset turbo timers for A.
//         if (isTurboA)
//         {
//             if (!isButtonA)
//             {
//                 turboDurationsA[index] = 0;
//                 turboPausesA[index] = 0;
//             }

//             // Check Pause
//             else if (turboPausesA[index] > 0)
//             {
//                 turboPausesA[index] -= timePassed;
//                 isButtonA = false;

//                 // Reset Pause
//                 if (turboPausesA[index] <= 0)
//                     turboPausesA[index] = 0;
//             }

//             // Add Duration
//             else
//             {
//                 turboDurationsA[index] += timePassed;

//                 // Reset Duration
//                 if (turboDurationsA[index] >= TURBO_DURATION_CYCLES * REGISTER_LENGTH)
//                 {
//                     turboDurationsA[index] = 0;
//                     turboPausesA[index] = TURBO_PAUSE_CYCLES * REGISTER_LENGTH;
//                 }
//             }
//         }

//         // Reset turbo timers for B.
//         if (isTurboB)
//         {
//             if (!isButtonB)
//             {
//                 turboDurationsB[index] = 0;
//                 turboPausesB[index] = 0;
//             }

//             // Check Pause
//             else if (turboPausesB[index] > 0)
//             {
//                 turboPausesB[index] -= timePassed;
//                 isButtonB = false;

//                 // Reset Pause
//                 if (turboPausesB[index] <= 0)
//                     turboPausesB[index] = 0;
//             }

//             // Add Duration
//             else
//             {
//                 turboDurationsB[index] += timePassed;

//                 // Reset Duration
//                 if (turboDurationsB[index] >= TURBO_DURATION_CYCLES * REGISTER_LENGTH)
//                 {
//                     turboDurationsB[index] = 0;
//                     turboPausesB[index] = TURBO_PAUSE_CYCLES * REGISTER_LENGTH;
//                 }
//             }
//         }
//     }

//     // Set Buttons
//     device.setButton(0, isButtonB);
//     device.setButton(1, isButtonA);
//     device.setButton(2, bitRead(nesRegister, SELECT_BUTTON));
//     device.setButton(3, bitRead(nesRegister, START_BUTTON));

//     // D-pad Buffering
//     bool right = bitRead(nesRegister, RIGHT_BUTTON);
//     bool left = bitRead(nesRegister, LEFT_BUTTON);
//     bool down = bitRead(nesRegister, DOWN_BUTTON);
//     bool up = bitRead(nesRegister, UP_BUTTON);

//     // Direction Vector
//     int axisX = right - left;
//     int axisY = down - up;

//     // Direction Normalized
//     int axisNormalizedX = (axisX * (halfRange)) + halfRange;
//     int axisNormalizedY = (axisY * (halfRange)) + halfRange;

//     // Set Directions
//     device.setXAxis(axisNormalizedX);
//     device.setYAxis(axisNormalizedY);

//     /*
//         // Convert bits to degrees.
//         int hatLinear = (right * up) + (right * !up * 3) + (down * right) + (down * !right * 5) + (left * down) + (left * !down * 7) + (up * left) + (up * !left) - 1;
//         int hat = (hatLinear < 0) ? -1 : hatLinear * 45;

//         // Set Hat
//         device.setHatSwitch(0, hat);
//     */

//     // Send State
//     device.sendState();
// }

// void UpdateCaseButtons()
// {
//     isFourPlayer = !digitalRead(TWOFOUR_PIN);

//     if (isFourPlayerLast != isFourPlayer)
//     {
//         isFourPlayerLast = isFourPlayer;

//         // 4 player turned off.
//         if (!isFourPlayer)
//         {
//             // Send off buttons.
//             for (int i = 2; i < 4; ++i)
//             {
//                 // Clear Memory
//                 registersLast[i] = 255;
//                 registers[i] = 255;

//                 // Reset Buttons
//                 Joystick_ device = usbJoysticks[i];
//                 device.setButton(0, false);
//                 device.setButton(1, false);
//                 device.setButton(2, false);
//                 device.setButton(3, false);

//                 // Reset Axis
//                 device.setXAxis(halfRange);
//                 device.setYAxis(halfRange);

//                 // Send State
//                 device.sendState();
//             }
//         }
//     }

//     isTurboA = digitalRead(TURBO_A_PIN);
//     isTurboB = digitalRead(TURBO_B_PIN);
// }

// void loop()
// {
//     // Check external switches.
//     UpdateCaseButtons();

//     // Reset the "4021" shift register on all connected controllers.
//     digitalWrite(LATCH_PIN, HIGH);
//     delayMicroseconds(PULSE_WIDTH_CD4021);
//     digitalWrite(LATCH_PIN, LOW);

//     byte controllerCount = (isFourPlayer) ? PORT_COUNT : 2;

//     // Get data from shift register(s).
//     for (int i = 0; i < CYCLE_COUNT_NES; ++i)
//     {
//         for (int j = 0; j < controllerCount; ++j)
//         {
//             // Buffer Data
//             bitWrite(registers[j], i, !digitalRead(playerPins[j]));

//             // Check State
//             if (registersLast[j] != registers[j])
//             {
//                 // Buffer state history.
//                 registersLast[j] = registers[j];

//                 // Send device state as soon as possible.
//                 SendDeviceState(j);
//             }
//         }

//         // Apply Clock
//         digitalWrite(CLOCK_PIN, HIGH);
//         delayMicroseconds(PULSE_WIDTH_CD4021);
//         digitalWrite(CLOCK_PIN, LOW);
//     }
// }