#define TURBO_DURATION_CYCLES 125
#define TURBO_PAUSE_CYCLES 125
#define REGISTER_LENGTH 8
#define AXIS_RADIUS 512
#define PORT_COUNT 4

#define BUTTON_COUNT_SNES 12
#define BUTTON_COUNT_NES 8

#define CYCLE_COUNT_NES 8
#define CYCLE_COUNT_SNES 16
#define CYCLE_COUNT_ARCADE24 24

#define PULSE_WIDTH_CD4021 20
#define PULSE_WIDTH_HC165 5

const bool ID_NES[8]        = { 0, 0, 0, 0, 0, 0, 0, 0};    // ID LOW, interpret as 8 cycle NES controller.
const bool ID_SNES[8]       = { 1, 1, 1, 1, 1, 1, 1, 1};    // ID HIGH, interpret as 16 cycle SNES controller with 12 buttons.
const bool ID_SNES_HC[8]    = { 0, 0, 0, 0, 1, 1, 1, 1};    // HC165 ID is cheaper to do with one side LOW and one side HIGH.
const bool ID_ARCADE24[8]   = { 0, 0, 0, 0, 0, 0, 0, 1};    // My 24-bit 2-player arcade board. 0.12ms to 1ms latency.

