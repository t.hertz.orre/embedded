#include <Arduino.h>
#include <Snes.h>

unsigned long blinkSecond = 10;
bool isLed = false;

void setup()
{
    setupLeds();
}

void loop()
{
    unsigned long blinkDelay = 1000 / blinkSecond;

    setLeds(LOW, isLed);
    delay(blinkDelay);

    setLeds(HIGH, isLed);
    delay(blinkDelay);

    isLed = !isLed;
}