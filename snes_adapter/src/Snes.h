// Methods
void setupLeds()
{
    pinMode(LED_BUILTIN_RX, OUTPUT);
    pinMode(LED_BUILTIN_TX, OUTPUT);

    digitalWrite(LED_BUILTIN_RX, LOW);
    digitalWrite(LED_BUILTIN_TX, LOW);
}

void setLeds(uint8_t val, bool isLed)
{
    if (isLed) digitalWrite(LED_BUILTIN_RX, val);
    else digitalWrite(LED_BUILTIN_TX, val);
}