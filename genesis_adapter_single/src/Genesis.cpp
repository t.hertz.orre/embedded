#include <Genesis.h>

Gamepad::Gamepad(uint8_t selectPin, uint8_t cycle)
{
    _selectPin = selectPin;
    _cycle = cycle;
}

void Gamepad::selectNextCycle()
{
    // Set new cycle.
    _cycle = _cycle + 1;
    if (_cycle >= GENESIS_CYCLE_COUNT) _cycle = 0;

    // Flip data select.
    digitalWrite(_selectPin, (_cycle % 2 == 0) ? HIGH : LOW);
}

int8_t Gamepad::cycle() { return _cycle; }