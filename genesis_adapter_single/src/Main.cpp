#include <Joystick.h>
#include <Arduino.h>
#include <Genesis.h>

#define DEBUG false

// Pins
#define DATA_0_PIN 14
#define DATA_1_PIN 15
#define DATA_2_PIN A0
#define DATA_3_PIN A1
#define DATA_4_PIN A2
#define DATA_5_PIN A3
#define SELECT_PIN 7

#define LED_RED 3
#define LED_GREEN 5
#define LED_BLUE 6

// Stuff that should be in genesis gamepad class.
int8_t cycle = -1;
bool inputRaw[GENESIS_DATA_PINS][GENESIS_CYCLE_COUNT];
bool inputBuffer[GENESIS_BUTTON_COUNT];
bool inputUpdated[GENESIS_BUTTON_COUNT];
bool isSixButton = false;
uint64_t inputCooldown = 0;
uint8_t ledState = 0;

#if DEBUG
uint32_t timeSpentSleeping = 0;
unsigned long millisLast = 0;
uint32_t inputUpdates = -1;
#endif

// Allocate USB Joysticks
Joystick_ usbJoysticks[GENESIS_GAMEPAD_COUNT] =
{
    Joystick_(0x03, JOYSTICK_TYPE_GAMEPAD, 8, 0, true, true, false, false, false, false, false, false, false, false, false),
};

void setup()
{
    // Pin Setup
    pinMode(DATA_0_PIN, INPUT);
    pinMode(DATA_1_PIN, INPUT);
    pinMode(DATA_2_PIN, INPUT);
    pinMode(DATA_3_PIN, INPUT);
    pinMode(DATA_4_PIN, INPUT);
    pinMode(DATA_5_PIN, INPUT);
    pinMode(SELECT_PIN, OUTPUT);
    pinMode(LED_RED,    OUTPUT);
    pinMode(LED_GREEN,  OUTPUT);
    pinMode(LED_BLUE,   OUTPUT);

    // Turn off leds.
    pinMode(LED_BUILTIN_TX, INPUT);
    pinMode(LED_BUILTIN_RX, INPUT);

    // Initial pin write states.
    digitalWrite(SELECT_PIN, LOW);

    // Initiate joystick to send state on manually.
    for (int i = 0; i < GENESIS_GAMEPAD_COUNT; ++i)
    {
        // Tell gamepads to send state manually.
        usbJoysticks[i].begin(false);

        // Adjusting max range to 1024 translates better into games made in by Unity.
        usbJoysticks[i].setXAxisRange(0, GENESIS_AXIS_RADIUS * 2);
        usbJoysticks[i].setYAxisRange(0, GENESIS_AXIS_RADIUS * 2);
    }

    // Initiate input buffer with false positive as first "history".
    inputBuffer[0] = true;

    #if DEBUG
    Serial.begin(9600);
    #endif
}

void setColor(int R, int G, int B)
{
    analogWrite(LED_RED,   R);
    analogWrite(LED_GREEN, G);
    analogWrite(LED_BLUE,  B);
}

void transferInputToUsb(uint8_t joystickIndex)
{
    // Direction Vector
    int axisX = inputBuffer[3] - inputBuffer[2];
    int axisY = inputBuffer[1] - inputBuffer[0];

    // Direction Normalized
    int axisNormalizedX = (axisX * (GENESIS_AXIS_RADIUS)) + GENESIS_AXIS_RADIUS;
    int axisNormalizedY = (axisY * (GENESIS_AXIS_RADIUS)) + GENESIS_AXIS_RADIUS;

    // Set joystick axis.
    Joystick_ joystick = usbJoysticks[joystickIndex];
    joystick.setXAxis(axisNormalizedX);
    joystick.setYAxis(axisNormalizedY);

    // Set button states.
    size_t buttonOffset = 4;
    for (size_t i = buttonOffset; i < GENESIS_BUTTON_COUNT; ++i)
        joystick.setButton(i - buttonOffset, inputBuffer[i]);

    // Send state to usb host.
    joystick.sendState();
}

bool wasInputUpdated(uint8_t cycleIndex)
{
    bool isUp, isDown, isLeft, isRight, isButtonA, isButtonB, isButtonC, isButtonStart, isButtonX, isButtonY, isButtonZ, isButtonMode;
    bool wasUpdated = false;

    // I tried sending joystick states for 2 days instead of waiting 190µs for each cycle.
    // It messes up the data whatever I do.

    switch (cycleIndex)
    {
        default: return false;
        case 8:
            isUp = (!inputRaw[0][0] || !inputRaw[0][1] || !inputRaw[0][2] || !inputRaw[0][3] || !inputRaw[0][4] || !inputRaw[0][8]);
            isDown = (!inputRaw[1][0] || !inputRaw[1][1] || !inputRaw[1][2] || !inputRaw[1][3] || !inputRaw[1][4] || !inputRaw[1][8]);
            isLeft = (!inputRaw[2][0] || !inputRaw[2][2] || !inputRaw[2][4] || !inputRaw[2][8]);
            isRight = (!inputRaw[3][0] || !inputRaw[3][2] || !inputRaw[3][4] || !inputRaw[3][8]);
            isButtonA = (!inputRaw[4][1] || !inputRaw[4][3] || !inputRaw[4][5] || !inputRaw[4][7]);
            isButtonB = (!inputRaw[4][0] || !inputRaw[4][2] || !inputRaw[4][4] || !inputRaw[4][6] || !inputRaw[4][8]);
            isButtonC = (!inputRaw[5][0] || !inputRaw[5][2] || !inputRaw[5][4] || !inputRaw[5][6] || !inputRaw[5][8]);
            isButtonStart = (!inputRaw[5][1] || !inputRaw[5][3] || !inputRaw[5][5] || !inputRaw[5][7]);
            isButtonX = false;
            isButtonY = false;
            isButtonZ = false;
            isButtonMode = false;

            // The 1650-50 3-button pad allows pushing right and left simultaniously writing to the 6-button data.
            if (isSixButton && !(isUp && isDown))
            {
                isButtonX = !inputRaw[2][6];
                isButtonY = !inputRaw[1][6];
                isButtonZ = !inputRaw[0][6];
                isButtonMode = !inputRaw[3][6];
            }

            if (inputBuffer[0] != isUp) { inputBuffer[0] = isUp; wasUpdated = true; }
            if (inputBuffer[1] != isDown) { inputBuffer[1] = isDown; wasUpdated = true; }
            if (inputBuffer[2] != isLeft) { inputBuffer[2] = isLeft; wasUpdated = true; }
            if (inputBuffer[3] != isRight) { inputBuffer[3] = isRight; wasUpdated = true; }
            if (inputBuffer[4] != isButtonA) { inputBuffer[4] = isButtonA; wasUpdated = true; }
            if (inputBuffer[5] != isButtonB) { inputBuffer[5] = isButtonB; wasUpdated = true; }
            if (inputBuffer[6] != isButtonC) { inputBuffer[6] = isButtonC; wasUpdated = true; }
            if (inputBuffer[7] != isButtonStart) { inputBuffer[7] = isButtonStart; wasUpdated = true; }
            if (inputBuffer[8] != isButtonX) { inputBuffer[8] = isButtonX; wasUpdated = true; }
            if (inputBuffer[9] != isButtonY) { inputBuffer[9] = isButtonY; wasUpdated = true; }
            if (inputBuffer[10] != isButtonZ) { inputBuffer[10] = isButtonZ; wasUpdated = true; }
            if (inputBuffer[11] != isButtonMode) { inputBuffer[11] = isButtonMode; wasUpdated = true; }
            return wasUpdated;
    }
}

bool cycleMultiplexer()
{
    // Increment Cycle
    ++cycle;
    if (cycle >= GENESIS_CYCLE_COUNT) cycle = 0;
    digitalWrite(SELECT_PIN, (cycle % 2 == 0) ? HIGH : LOW);

    // Update buffer here for now.
    inputRaw[0][cycle] = (bool)digitalRead(DATA_0_PIN);
    inputRaw[1][cycle] = (bool)digitalRead(DATA_1_PIN);
    inputRaw[2][cycle] = (bool)digitalRead(DATA_2_PIN);
    inputRaw[3][cycle] = (bool)digitalRead(DATA_3_PIN);
    inputRaw[4][cycle] = (bool)digitalRead(DATA_4_PIN);
    inputRaw[5][cycle] = (bool)digitalRead(DATA_5_PIN);

    return wasInputUpdated(cycle);
}

#if DEBUG
void printBuffer()
{
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.print("    Updates ");
    Serial.print(inputUpdates);

    if (isSixButton)
    {
        // float cycleDurationMS = GENESIS_SIX_BUTTON_DELAY * GENESIS_CYCLE_COUNT * 0.001;
        // float cycleDurationMS = 1000 / (float)((GENESIS_INPUT_COOLDOWN_SIX / 60) / GENESIS_CYCLE_COUNT);
        Serial.print(", 6-button, latency ");
        // Serial.print(cycleDurationMS);
        Serial.print(1.52);
        Serial.print("ms to 2ms.");
        Serial.println();
    }

    else
    {
        float cycleDurationMS = 1000 / (float)((GENESIS_INPUT_COOLDOWN_THREE / 60) / GENESIS_CYCLE_COUNT);
        Serial.print(", 3-button, latency ");
        Serial.print(cycleDurationMS);
        Serial.print("ms to 1ms.");
        Serial.println();
    }

    for (size_t y = 0; y < GENESIS_CYCLE_COUNT; ++y)
    {
        Serial.println();
        Serial.print(y);
        Serial.print("   ");

        for (size_t x = 0; x < GENESIS_DATA_PINS; ++x)
        {
            Serial.print("[ ");
            Serial.print(inputRaw[x][y]);
            Serial.print(" ]");
        }
    }

    Serial.println();
    Serial.println();
    Serial.print("    ");

    for (size_t i = 0; i < GENESIS_BUTTON_COUNT; ++i)
    {
        Serial.print("[ ");

        if (!inputBuffer[i]) Serial.print(" ");
        else
        {
            switch (i)
            {
                case 0: Serial.print("↑"); break;
                case 1: Serial.print("↓"); break;
                case 2: Serial.print("←"); break;
                case 3: Serial.print("→"); break;
                case 4: Serial.print("A"); break;
                case 5: Serial.print("B"); break;
                case 6: Serial.print("C"); break;
                case 7: Serial.print("►"); break;
                case 8: Serial.print("X"); break;
                case 9: Serial.print("Y"); break;
                case 10: Serial.print("Z"); break;
                case 11: Serial.print("☼"); break;
                default: Serial.print("•"); break;
            }
        }

        Serial.print(" ]");
    }

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
}

void printDebugInput()
{
    int inputCount = 0;
    for (size_t i = 0; i < GENESIS_BUTTON_COUNT; ++i)
        if (inputBuffer[i]) ++inputCount;

    if (inputCount > 1)
    {
        Serial.print("    ");
        Serial.println(inputCount);
    }
}
#endif

bool isDisconnected()
{
    for (size_t y = 0; y < GENESIS_CYCLE_COUNT; ++y)
        for (size_t x = 0; x < GENESIS_DATA_PINS; ++x)
            if (inputRaw[x][y]) return false;

    return true;
}

void delayInput(uint64_t ms)
{
    cycle = -1;
    delay(ms);
}

void resetJoystick(Joystick_ joystick)
{
    joystick.setXAxis(GENESIS_AXIS_RADIUS);
    joystick.setYAxis(GENESIS_AXIS_RADIUS);
    for (size_t i = 0; i < GENESIS_BUTTON_COUNT - 4; ++i)
        joystick.setButton(i, 0);

    joystick.sendState();
}

void loop()
{
    // MK-1653-50 Genesis 6-button controller can't poll at the same rate as the 3-button.
    // It has to be delayed.
    // TODO Find out why reset to 6-button delay on disconnect somehow acts as the mode-switch from 6 to 3 button.
    isSixButton = (!inputRaw[0][5] && !inputRaw[1][5]);

    // Increment multiplexer cycle.
    bool wasUpdated = cycleMultiplexer();

    // Delay if 6-button controller.
    if (isSixButton) delayMicroseconds(GENESIS_SIX_BUTTON_COOLDOWN);

    if (wasUpdated)
    {
        inputCooldown = ((isSixButton) ? GENESIS_INPUT_COOLDOWN_SIX : GENESIS_INPUT_COOLDOWN_THREE) * GENESIS_INPUT_COOLDOWN_MINUTES;
        ledState = 1;

        #if DEBUG
        ++inputUpdates;
        timeSpentSleeping = 0;
        // printDebugInput();
        printBuffer();
        #endif

        transferInputToUsb(0);
    }

    if (isDisconnected())
    {
        #if DEBUG
        Serial.println("disconnected");
        inputUpdates = -1;
        #endif

        if (ledState > 0) resetJoystick(usbJoysticks[0]);

        ledState = 0;
        delayInput(GENESIS_CONNECT_COOLDOWN);
    }

    if (inputCooldown > 0)
    {
        --inputCooldown;

    #if DEBUG
        unsigned long ms = millis();
        unsigned long delta = ms - millisLast;
        millisLast = ms;
        timeSpentSleeping += delta;
    #endif
    }

    if (inputCooldown == 0 && cycle == GENESIS_CYCLE_COUNT - 1)
    {
    #if DEBUG
        uint32_t seconds = timeSpentSleeping / 1000;
        uint32_t decimals = timeSpentSleeping - (seconds * 1000);
        Serial.print("sleep after ");
        Serial.print(seconds);
        Serial.print(".");
        Serial.print(decimals);
        Serial.print(" seconds");
        Serial.println();
    #endif

        ledState = 2;
        delayInput(GENESIS_INPUT_SLEEP);
    }

    switch (ledState)
    {
        // Disconnected
        case 0: setColor(60, 1, 0); break;

        // Ready
        case 1: setColor(10, 35, 0); break;

        // Standby
        case 2: setColor(0, 0, 2); break;
    }
}