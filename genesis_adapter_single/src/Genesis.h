#include <Arduino.h>

#define GENESIS_DATA_PINS 6
#define GENESIS_CYCLE_COUNT 9
#define GENESIS_GAMEPAD_COUNT 1
#define GENESIS_AXIS_RADIUS 512
#define GENESIS_CONNECT_COOLDOWN 1000
#define GENESIS_BUTTON_COUNT 12
#define GENESIS_INPUT_SLEEP 800

// 6-button gamepads need 190 µs to 220 µs between cycles for correct data to show.
#define GENESIS_SIX_BUTTON_COOLDOWN 190
#define GENESIS_INPUT_COOLDOWN_SIX 224000
#define GENESIS_INPUT_COOLDOWN_THREE 775000
#define GENESIS_INPUT_COOLDOWN_MINUTES 30

// MK-1653-50 6-button controller requires 138 microseconds delay between cycles and the full 9 cycle count.

// The controller component should support 16Mhz, meaning I should be able to
// run jobs alongside the chip loop.

class Gamepad
{
private:
    uint8_t _selectPin;

public:
    int8_t _cycle;
    bool _inputBuffer[GENESIS_DATA_PINS][GENESIS_CYCLE_COUNT];
    Gamepad(uint8_t selectPin, uint8_t cycle);
    void selectNextCycle();
    int8_t cycle();
};