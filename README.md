Me figuring out what the perfect controller was from the 80's.

What's worth preserving? Because the truth is that these machines fade and there has been a lot of research on them. The most important thing, how to communicate with these devices:
- NES, single 8-bit 4021 shift-register. 8 Cycle.
- SNES, dual 8-bit 4021 shift-register in cascade. 16 Cycle.
- Genesis/MD, single 4-line to 2-line 74HC157 multiplexer, dual for 6-button. 9 Cycle.
- Saturn, dual 4-line to 1-line 74HC153 multiplexer. 4 Cycle.

Latencies so far:
- Nes standard ~0.08ms to 1ms.
- SNES/FMC standard ~0.16ms to 1ms.
- Genesis/MD 3-button 0.77ms to 1ms.
- Genesis/MD 6-button, 1.52ms to 2ms.
- Saturn MK2 0.29ms to 1ms.

Competition:
- [rpubs.com](https://rpubs.com/misteraddons/inputlatency)


<!-- ![Test](/readme/sega_mk-1654.jpg "What is this?"){width=25%} -->


<!--
What you need:
- Classic good looking adapter.
- ATmega32U4 in any cloned format.
- GX12 4-pin docking connector set.
- Nylon sleeved 22AWG Micro USB cable.


Each build also needs basic components and tools. -->

A mistake Nintendo did I think, was not making their glorious NES connector with a device ID on pin 7. Instead, they opted for a Zapper trigger when it could have been on pin 4 with a console input bypass. On that pin I'd add another register for devices needing more than 16 bits. Just keeping pin 7 HIGH could make it into a SNES controller for free.