1   red     +5V
2   white   data_1
3   yellow  data_0
4   orange  select_0
5   blue    select_1
6   green   +5V             keep high always
7   purple  data_3
8   brown   data_2
9   black   ground


s0  s1      d0  d1  d2  d3
L   L       Z   Y   X   R
H   L       B   C   A   St
L   H       ↑   ↓   ←   →
H   H       -   -   *   L


* Is keept high on MK-80313.